package com.example.viewmodeltutorial;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import java.util.Random;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class DataGenerator extends ViewModel {
    private String Tag = this.getClass().getSimpleName();
    private  String myRandomNumber;

    public String getNumber() {
        Log.i(TAG,"Get number");
        if(myRandomNumber == null){
            createNumber();
        }
        return myRandomNumber;
    }

    private void createNumber(){
        Log.i(TAG, "Create new number" );
        Random random = new Random();
        myRandomNumber = "Number: " + (random.nextInt(100-1)+1);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    Log.i(TAG,"ViewModel Destroyed");}
}
